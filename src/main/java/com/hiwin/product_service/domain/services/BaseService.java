package com.hiwin.product_service.domain.services;

import com.hiwin.product_service.domain.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public abstract class BaseService<T> {
    @Autowired
    protected ProductRepository productRepository;

    public abstract T getById(long id);

    public abstract T create(T object);

    public abstract T update(long id, T product);

    public abstract Page<T> getAll(Pageable pageable);

    public abstract boolean delete(long id);

}
