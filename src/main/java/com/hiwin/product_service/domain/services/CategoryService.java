package com.hiwin.product_service.domain.services;

import com.hiwin.product_service.domain.entities.postgres.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends BaseService<Category> {

    @Override
    public Category getById(long id) {
        return null;
    }

    @Override
    public Category create(Category object) {
        return null;
    }

    @Override
    public Category update(long id, Category product) {
        return null;
    }

    @Override
    public Page<Category> getAll(Pageable pageable) {
        return null;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }
}
