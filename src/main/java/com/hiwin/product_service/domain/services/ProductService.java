package com.hiwin.product_service.domain.services;

import com.hiwin.product_service.app.dto.ProductChangeStateDto;
import com.hiwin.product_service.domain.constants.ProductConstant;
import com.hiwin.product_service.domain.entities.postgres.Product;
import com.hiwin.product_service.domain.exceptions.ExceptionMessage;
import com.hiwin.product_service.domain.types.ProductState;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@Service
public class ProductService extends BaseService<Product> {

    /**
     * find by id and caching
     *
     * @param id id product
     * @return Product.class
     */
    @Cacheable(value = "product", key = "#id")
    @Override
    public Product getById(long id) {
        return productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, ExceptionMessage.PRODUCT_NOT_FOUND));
    }

    /**
     * Create new product
     *
     * @param product product data
     * @return product new
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED,
            rollbackFor = Exception.class,
            isolation = Isolation.SERIALIZABLE)
    public Product create(Product product) {
        //TODO: handle category
        product.inferProperties();
        return productRepository.save(product);
    }

    /**
     * update product by id
     *
     * @param id      id product
     * @param product product update
     * @return product update
     */
    @Override
    @CacheEvict(value = "product", key = "#id")
    @Transactional(propagation = Propagation.REQUIRED,
            rollbackFor = Exception.class,
            isolation = Isolation.SERIALIZABLE)
    public Product update(long id, Product product) {
        Product productOld = productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, ExceptionMessage.PRODUCT_NOT_FOUND));
        if (product.getQuantity() <= productOld.getSoldQuantity())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ExceptionMessage.ERROR_QUANTITY);
        product.setId(id);
        product.setSoldQuantity(productOld.getSoldQuantity());
        product.setUpdatedAt(LocalDateTime.now(ProductConstant.ZONE_ID));
        return productRepository.save(product);
    }

    /**
     * Change state for product
     *
     * @param id
     * @param dto
     * @return
     */
    @CacheEvict(value = "product", key = "#id")
    @Transactional(propagation = Propagation.REQUIRED,
            rollbackFor = Exception.class,
            isolation = Isolation.SERIALIZABLE)
    public Boolean changeState(long id, ProductChangeStateDto dto) {
        Product product = productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, ExceptionMessage.PRODUCT_NOT_FOUND));
        product.setState(dto.getState());
        productRepository.save(product);
        return true;
    }

    /**
     * Get all by page
     *
     * @param pageable
     * @return
     */
    @Override
    @Cacheable(value = "products", key = "{ #pageable.pageNumber, #pageable.pageSize }")
    public Page<Product> getAll(Pageable pageable) {
        return productRepository.findByStateOrderByCreatedAtDesc(ProductState.PUBLIC, pageable);
    }

    /**
     * Delete product
     *
     * @param id
     * @return
     */
    @CacheEvict(value = "product", key = "#id")
    @Transactional(propagation = Propagation.REQUIRED,
            rollbackFor = Exception.class,
            isolation = Isolation.SERIALIZABLE)
    @Override
    public boolean delete(long id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, ExceptionMessage.PRODUCT_NOT_FOUND));
        product.setState(ProductState.DELETED);
        productRepository.save(product);
        return true;
    }

}
