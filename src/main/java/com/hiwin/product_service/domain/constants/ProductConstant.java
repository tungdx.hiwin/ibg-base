package com.hiwin.product_service.domain.constants;

import java.time.ZoneId;

public class ProductConstant {
    public static ZoneId ZONE_ID = ZoneId.of("GMT");
}
