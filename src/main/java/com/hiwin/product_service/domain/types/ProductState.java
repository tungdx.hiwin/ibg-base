package com.hiwin.product_service.domain.types;


public enum ProductState {
    PENDING,
    PUBLIC,
    DELETED
}
