package com.hiwin.product_service.app.dto;

import com.hiwin.product_service.domain.types.ProductState;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.validation.Valid;

@Data
@NoArgsConstructor
@Log4j2
@Valid
public class ProductChangeStateDto {
    @NotNull
    private ProductState state;
}
