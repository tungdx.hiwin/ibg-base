package com.hiwin.product_service.app.data;

import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class MetaData {
    private int page;
    private int size;
    private long total;
    private long totalPages;

    public static MetaData of(Page page) {
        MetaData metadata = new MetaData();
        metadata.setSize(page.getSize());
        metadata.setTotal(page.getTotalElements());
        metadata.setPage(page.getNumber());
        metadata.setTotalPages(page.getTotalPages());
        return metadata;
    }
}
