package com.hiwin.product_service.app.controller;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/internal")
public class InternalController {
    /**
     * Quantity change
     *
     * @param dto
     * @param id
     * @return
     * @throws Exception
     */
    @PutMapping("/{id}/qty")
    public void changeQuantity() throws Exception {
        // TODO
    }
}
