package com.hiwin.product_service.app.controller;

import com.hiwin.product_service.domain.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {
    @Autowired
    protected ProductService productService;

//    @Autowired
//    protected ProductMapper productMapper;

    protected ModelMapper modelMapper = new ModelMapper();

}
